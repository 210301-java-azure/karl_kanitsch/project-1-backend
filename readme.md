# Project 0 - HR Management API  
# For Users
## Resources / Endpoints   

[Backend Address](52.150.14.143)

<!-- |Resource|Method|Role|Result|
|---|---|---|---|
|register|Get|None|Allows a new user account to be created|
|register|Post|Admin|Allows an Admin to link a user account to an employee record|
|login|Get|Any|If a valid auth-token is found, alerts user of login and privilege|
|login|Post|Any|Attempts to log the user in to their account and returns an auth-token if successful|
|user/change-password|Post|User|Attempts to change the password on the user account| -->
Parameters marked with an askterisk (*) are required.  
`register`  
|Method|Role|Form Parameter|Result|
|---|---|---|---|
|Post|None|username*, password*, email*|Registers a new user account with the supplied credentials|
  

`login`  
|Method|Role|Form Parameters|Result|
|---|---|---|---|
|Get|Any||If a valid auth-token is found in the header, alerts user of login and privilege level|
|Post|Any|username*, password*|Attempts to log into the associated user account and if successful, returns an auth-token|  

`user`  
|Method|Role|Query Parameters|Form Parameters|Result|
|---|---|---|---|---|
|Get|Admin|||Returns a list of all user accounts|
|Get|User, HR|||Returns the logged in user account information|
|Get|Admin|userid, privilege, employee-id||Returns a list of all user accounts that match any included query parameters.|  
|Post|Admin||userid*, employee-id*|Links the user account specified to the employee specified|  

`user/change-password`  
|Method|Role|Form Parameters|Result|
|---|---|---|---|
|Post|User+|username*, password*, new-password*|Changes the password on the user account|  

`employee`  
|Method|Role|Form Parameters|Result|
|---|---|---|---|
|Get|User, Admin||Returns the employee information associated with the logged in user account|
|Get|HR||Returns all employee records from the database|
|Put|HR|first-name*, last-name*, pay-rate*, federal_id*|Adds a new employee to the database|
|Delete|HR|employee-id*|Removes an employee record from the database|  

`employee/{id}`  
|Method|Role|Result|
|---|---|---|
|Get|HR|Retrieves the employee information matching the specified employee-id|
|Get|User, Admin|Retrieves the employee information if the employee linked to the currently logged in user account matches the employee-id entered|  



# For Developers  
## Data Structure  
![ERD](./docs/ERD.png)  
## Classes Listed  
### Controllers 
Controllers use Javalin to process HTTP requests. Most methods in these classes are Javalin handlers, with a few utility methods. 
- [AuthController](./docs/AuthController.md)
- [EmployeeController](./docs/EmployeeController.md)
- [UserController](./docs/UserController.md)
  
### Services  
Services handle most of the java logic and provide a conduit between the Controllers and the DAOs.
- [EmployeeService](./docs/EmployeeService.md)
- [PrivilegeService](./docs/PrivilegeService.md)
- [UserService](./docs/UserService.md)

### DAOs
DAOs (Data Access Objects) manage our data layer and interact with the SQL database.


