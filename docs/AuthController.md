## Dependencies  
[SL4J Logger](http://www.slf4j.org/)  
[Javalin](https://javalin.io/)


## Fields  

|Access|Static|Final|DataType|Name|
|---|---|---|---|---|
|private|<center>F</center>|<center>F</center>|String|username|
|private|<center>F</center>|<center>F</center>|String|password|
|private|<center>F</center>|<center>F</center>|SL4J Logger|logger|
|private|<center>F</center>|<center>F</center>|[UserService](./docs/UserService.md)|userService|
|private|<center>F</center>|<center>F</center>|[PrivilegeService](./docs/PrivilegeService.md)|privilegeService|
  
## Methods  

|Access|Static|Signature|Return|Summary|  
|---|---|---|---|---| 
|public|<center>F</center>|handleAuthenticateUser(Context ctx)|void|Authenticates user and returns an authentication JWT|
|public|<center>F</center>|handleLoginGet(Context ctx)|void|Gets authentication token from the request header and if authorized as a user, welcomes the guest|
|public|<center>F</center>|getPrivilege(Context ctx)|int|Gets authentication token from the request header and returns the privilege level, or 0 if no privilege level is found|
|public|<center>F</center>|getUserName(Context ctx)|String|Gets authentication token from the request header and returns the username|
|public|<center>F</center>|hasHRAccess(Context ctx)|boolean|Calls getPrivilege method and returns True if user has HR Access privileges|
|public|<center>F</center>|hasUserAccess(Context ctx)|boolean|Calls getPrivilege method and returns True if user account exists|

[readme](../readme.md)





