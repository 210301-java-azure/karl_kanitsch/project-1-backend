This class handles all BCrypt password hashing and JSON Web Token creation and signing.

## Dependencies  
[SL4J Logger](http://www.slf4j.org/)  
[Auth0 JWT](https://search.maven.org/artifact/com.auth0/java-jwt/3.14.0/jar)  
[Javalin](https://javalin.io/)  
[Spring Security](https://spring.io/projects/spring-security)


## Fields  

|Access|Static|Final|DataType|Name|
|---|---|---|---|---|
|private|<center>F</center>|<center>T</center>|SL4J Logger|logger|
|private|<center>F</center>|<center>T</center>|[UserDAO](./docs/UserDAO.md)|uDAO|
|private|<center>F</center>|<center>T</center>|java.security.Key|key|

  
## Methods  

|Access|Static|Signature|Return|Summary|  
|---|---|---|---|---| 
|public|<center>F</center>|authenticate(String username, String password)|int|Hashes submitted password, compares it to the stored value and returns the privilege level of the user|
|public|<center>F</center>|generateAuthToken(String username, int privilege)|String|Creates a JSON with the submitted username and password, encodes and signs it, then returns the JWS as a String|
|public|<center>F</center>|validateAuthToken(String authToken)|Map\<String,Object\>|Decodes the JWS passing its stored values into a Map which is returned|
|public|<center>F</center>|addNewUser(String username, String password, String email)|int|Adds a new user account with the provided credentials and returns an int indicating the status|
|public|<center>F</center>|changeUserPassword(String username, String password, String newPassword)|int|Calls authenticate method and if user is valid, changes users password|
|public|<center>F</center>|getEmployeeID(String username)|int|Retrieves the users employee-id from the database and returns it|
|public|<center>F</center>|hashAllPassword()|void|Gets all passwords from the database, hashes them and resubmits. Only used when database is reset with stock data.|


[readme](../readme.md)



