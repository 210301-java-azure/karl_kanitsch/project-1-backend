drop table if exists employees;
create table employees (
	employee_id SERIAL PRIMARY KEY,
	first_name VARCHAR NOT NULL,
	last_name VARCHAR NOT NULL,
	hire_date DATE NOT NULL,
	pay_rate DECIMAL NOT NULL
);
