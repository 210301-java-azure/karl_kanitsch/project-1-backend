package dev.kkanit.dao;

import dev.kkanit.models.UserAccount;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class UserDAOTest {

    UserDAO uDAO = new UserDAOImpl();

    @Disabled
    @Test
    public void testAddNewUser(){
        removeTestUser();
        UserAccount newUser = uDAO.addNewUser("testUser","fakePassword","fake@email.com");
        assertAll(
                ()->assertNotNull(newUser),
                ()->assertTrue(newUser.getUsername()=="testUser"),
                ()->assertTrue(newUser.getPassword()=="fakePassword"),
                ()->assertTrue(newUser.getUserID()>0),
                ()->assertTrue(newUser.getEmail()=="fake@email.com")
        );
    }

    @Disabled
    @Test
    public void testGetAllUsernames(){
        addTestUser();
        List<String> usernames = uDAO.getAllUsernames();
        assertAll(
                ()->assertTrue(usernames.size() > 0),
                ()->assertTrue(usernames.contains("testUser"))
        );
    }

    @Disabled
    @Test
    public void testGetAllEmails(){
        addTestUser();
        List<String> emails = uDAO.getAllEmails();
        assertAll(
                ()->assertTrue(emails.size() > 0),
                ()->assertTrue(emails.contains("fake@email.com"))
        );
    }

    @Disabled
    @Test
    public void testGetUserAccount(){
        addTestUser();
        UserAccount userAccount = uDAO.getUserAccount("testUser");
        assertAll(
                ()->assertEquals("testUser",userAccount.getUsername()),
                ()->assertEquals("fake@email.com",userAccount.getEmail()),
                ()->assertTrue(userAccount.getUserID()>0)
        );
    }

    @Disabled
    @Test
    public void testGetAllUserAccounts(){
        addTestUser();
        List<UserAccount> userAccounts = uDAO.getAllUserAccounts();
        assertAll(
                ()->assertTrue(userAccounts.size()>0)
        );
    }

    @Disabled
    @Test
    public void testGetFilteredUserAccounts(){
        addTestUser();
        Map<String,String> queryMap = new HashMap<>();
        queryMap.put("employeeID","_GT_0");
        queryMap.put("username","_LIKE_%estUse%");
        List<UserAccount> userAccounts = uDAO.getFilteredUserAccounts(queryMap);
        System.out.println("Returned user account username: "+userAccounts.get(0).getUsername());
        assertAll(
                ()->assertTrue(userAccounts.size()>0),
                ()->assertEquals("testUser",userAccounts.get(0).getUsername())
        );
    }

    @Disabled
    @Test
    public void testGetUserPassword(){
        addTestUser();
        assertEquals("fakePassword",uDAO.getUserPassword("testUser"));
    }

    @Disabled
    @Test
    public void testChangeUserPassword(){
        addTestUser();
        int userID = uDAO.getUserAccount("testUser").getUserID();
        assertAll(
                ()->assertTrue(uDAO.changeUserPassword(userID,"fakePassword2")),
                ()->assertEquals("fakePassword2",uDAO.getUserPassword("testUser"))
        );
    }

    @Disabled
    @Test
    public void testSetRole(){
        addTestUser();
        int userID = uDAO.getUserAccount("testUser").getUserID();
        assertAll(
                ()->assertTrue(uDAO.setRole(userID,2)),
                ()->assertEquals(2,uDAO.getRole("testUser")),
                ()->assertTrue(uDAO.setRole(userID,1))
        );
    }

    @Disabled
    @Test
    public void testGetRole(){
        addTestUser();
        assertEquals(1,uDAO.getRole("testUser"));
    }

    @Disabled
    @Test
    public void testSetEmployeeID(){
        addTestUser();
        int userID = uDAO.getUserAccount("testUser").getUserID();
        assertAll(
                ()->assertTrue(uDAO.setEmployeeID(userID,65)),
                ()->assertEquals(65,uDAO.getEmployeeID("testUser"))
        );
    }

    @Disabled
    @Test
    public void testGetEmployeeID(){
        addTestUser();
        int userID = uDAO.getUserAccount("testUser").getUserID();
        uDAO.setEmployeeID(userID,65);
        assertEquals(65,uDAO.getEmployeeID("testUser"));
    }

    @Disabled
    @Test
    public void testDeleteUser(){
        addTestUser();
        UserAccount userAccount = uDAO.getUserAccount("testUser");
        assertAll(
                ()->assertTrue(uDAO.deleteUser(userAccount)),
                ()->assertNull(uDAO.getUserAccount("testUser"))
        );
    }

    // Below are utility classes used to ensure that our testing environment is set up correctly
    private void removeTestUser(){
        UserAccount testUser = uDAO.getUserAccount("testUser");
        if(testUser!=null)
            uDAO.deleteUser(testUser);
    }
    private void addTestUser(){
        if(uDAO.getUserAccount("testUser")==null)
            uDAO.addNewUser("testUser","fakePassword","fake@email.com");
    }
}
