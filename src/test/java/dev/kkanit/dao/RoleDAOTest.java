package dev.kkanit.dao;

import dev.kkanit.models.Role;
import dev.kkanit.utilities.DBPopulator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RoleDAOTest {

    private static final RoleDAO rDAO = new RoleDAOImpl();
    private static int addedRoleID;

//    @BeforeAll
//    public static void resetDB() { DBPopulator.resetDB(); }

    @Disabled
    @Test
    public void testAddRole(){
        Role testRole = new Role(0,"testRole",true,false,true);
        Role returnedRole = rDAO.addRole(testRole);
        addedRoleID = returnedRole.getRoleID();
        assertAll(
                ()->assertNotEquals(0,returnedRole.getRoleID()),
                ()->assertTrue(testRole.identical(returnedRole))
        );
    }

    @Disabled
    @Test
    public void testGetRoleName(){
        assertAll(
                ()->assertEquals("User",rDAO.getRoleName(1)),
                ()->assertEquals("HR Manager",rDAO.getRoleName(2)),
                ()->assertEquals("System Administrator",rDAO.getRoleName(3)),
                ()->assertEquals(null,rDAO.getRoleName(0))
        );
    }

    @Disabled
    @Test
    public void testHasUserAccess(){
        assertAll(
                ()->assertFalse(rDAO.hasUserAccess(0)),
                ()->assertTrue(rDAO.hasUserAccess(1)),
                ()->assertTrue(rDAO.hasUserAccess(2)),
                ()->assertTrue(rDAO.hasUserAccess(3))
        );
    }

    @Disabled
    @Test
    public void testHasHRAccess(){
        assertAll(
                ()->assertFalse(rDAO.hasHRAccess(0)),
                ()->assertFalse(rDAO.hasHRAccess(1)),
                ()->assertTrue(rDAO.hasHRAccess(2)),
                ()->assertFalse(rDAO.hasHRAccess(3))
        );
    }

    @Disabled
    @Test
    public void testHasAdminAccess(){
        assertAll(
                ()->assertFalse(rDAO.hasAdminAccess(0)),
                ()->assertFalse(rDAO.hasAdminAccess(1)),
                ()->assertFalse(rDAO.hasAdminAccess(2)),
                ()->assertTrue(rDAO.hasAdminAccess(3))
        );
    }

    @Disabled
    @Test
    public void testDeleteRole(){
        Role testRole = new Role(addedRoleID,"testRole",true,false,true);
        assertTrue(rDAO.deleteRole(testRole));
    }
}
