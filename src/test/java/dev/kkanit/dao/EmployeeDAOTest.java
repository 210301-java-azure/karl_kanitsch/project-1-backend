package dev.kkanit.dao;

import dev.kkanit.models.Employee;
import dev.kkanit.models.UserAccount;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeDAOTest {

    EmployeeDAO eDAO = new EmployeeDAOImpl();

    @Disabled
    @Test
    public void testGetAllEmployees(){
        addTestEmployee();
        List<Employee> employees = eDAO.getAllEmployees();
        assertAll(
                ()->assertTrue(employees.size()>0),
                ()->assertNotNull(eDAO.getEmployeeByFID("fakeFederalID"))
        );
    }

    @Disabled
    @Test
    public void testGetEmployee(){
        addTestEmployee();
        assertAll(
                ()->assertEquals(null,eDAO.getEmployee(0)),
                ()->assertEquals("Fake",eDAO.getEmployeeByFID("fakeFederalID").getFirstName())
        );
    }

    @Disabled
    @Test
    public void testAddEmployee(){
        String firstName = "Fake";
        String lastName = "Employee";
        double payRate = -25.50;
        String federalID = "fakeFederalID";
        int employeeID = eDAO.addEmployee(firstName,lastName,payRate,federalID);
        assertAll(
                ()->assertNotEquals(0,employeeID),
                ()->assertEquals(firstName,eDAO.getEmployee(employeeID).getFirstName()),
                ()->assertEquals(lastName,eDAO.getEmployee(employeeID).getLastName()),
                ()->assertEquals(federalID,eDAO.getEmployee(employeeID).getFederalID()),
                ()->assertTrue(eDAO.removeEmployee(employeeID))
        );
    }

    @Disabled
    @Test
    public void testRemoveEmployee(){
        addTestEmployee();
        int employeeID = eDAO.getEmployeeByFID("fakeFederalID").getEmployeeID();
        assertAll(
                ()->assertTrue(eDAO.removeEmployee(employeeID)),
                ()->assertNull(eDAO.getEmployee(employeeID))
        );
    }

    @Disabled
    @Test
    public void testUpdateEmployee(){
        addTestEmployee();
        Employee testEmployee = eDAO.getEmployeeByFID("fakeFederalID");
        testEmployee.setPayRate(45.23);
        testEmployee.setAccruedPTO(14.56);
        eDAO.updateEmployee(testEmployee);
        assertAll(
                ()->assertEquals(45.23,eDAO.getEmployeeByFID("fakeFederalID").getPayRate()),
                ()->assertEquals(14.56,eDAO.getEmployeeByFID("fakeFederalID").getAccruedPTO())
        );
    }

    // Below are utility classes used to ensure that our testing environment is set up correctly
    private void removeTestEmployee(){
        Employee testEmployee = eDAO.getEmployeeByFID("fakeFederalID");
        if(testEmployee!=null)
            eDAO.removeEmployee(testEmployee.getEmployeeID());
    }
    private void addTestEmployee(){
        if(eDAO.getEmployeeByFID("fakeFederalID")==null)
            eDAO.addEmployee("Fake","Employee",-25.50, "fakeFederalID");
    }
}
