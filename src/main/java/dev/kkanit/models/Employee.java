package dev.kkanit.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="employee")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="employee_id")
    private int employeeID;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    @Column(name="hire_date")
    private String hireDate;
    @Column(name="termination_date")
    private String terminationDate;
    private boolean active;
    @Column(name="pay_rate")
    private double payRate;
    @Column(name="accrued_pto")
    private double accruedPTO;
    @Column(name="federal_id",unique = true)
    private String federalID;

    public Employee() {}

    public Employee(int employeeID, String firstName, String lastName, String hireDate, String terminationDate,
                    boolean active, double payRate, double accruedPTO, String federalID) {
        this.employeeID = employeeID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.hireDate = hireDate;
        this.terminationDate = terminationDate;
        this.active = active;
        this.payRate = payRate;
        this.accruedPTO = accruedPTO;
        this.federalID = federalID;
    }

    public Employee(String firstName, String lastName, String hireDate, String terminationDate,
                    boolean active, double payRate, double accruedPTO, String federalID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.hireDate = hireDate;
        this.terminationDate = terminationDate;
        this.active = active;
        this.payRate = payRate;
        this.accruedPTO = accruedPTO;
        this.federalID = federalID;
    }

    public Employee(String firstName, String lastName, double payRate, String federalID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.payRate = payRate;
        this.federalID = federalID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHireDate() {
        return hireDate;
    }

    public void setHireDate(String hireDate) {
        this.hireDate = hireDate;
    }

    public String getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(String terminationDate) {
        this.terminationDate = terminationDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getPayRate() {
        return payRate;
    }

    public void setPayRate(double payRate) {
        this.payRate = payRate;
    }

    public double getAccruedPTO() {
        return accruedPTO;
    }

    public void setAccruedPTO(double accruedPTO) {
        this.accruedPTO = accruedPTO;
    }

    public String getFederalID() {
        return federalID;
    }

    public void setFederalID(String federalID) {
        this.federalID = federalID;
    }

    public String toString() {
        return this.firstName + "\t" + this.lastName + "\t" + this.active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeID == employee.employeeID && active == employee.active && Double.compare(employee.payRate, payRate) == 0 && Double.compare(employee.accruedPTO, accruedPTO) == 0 && Objects.equals(firstName, employee.firstName) && Objects.equals(lastName, employee.lastName) && Objects.equals(hireDate, employee.hireDate) && Objects.equals(terminationDate, employee.terminationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeID, firstName, lastName, hireDate, terminationDate, active, payRate, accruedPTO);
    }
}
