package dev.kkanit.dao;

import dev.kkanit.models.UserAccount;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface UserDAO {

    UserAccount addNewUser(String username, String password, String email);
    List<String> getAllUsernames();
    List<String> getAllEmails();
    UserAccount getUserAccount(String username);
    UserAccount getUserAccount(int userID);
    List<UserAccount> getAllUserAccounts();
    List<UserAccount> getFilteredUserAccounts(Map<String,String> queryMap);
    String getUserPassword(String username);
    boolean changeUserPassword(int userID, String password);
    boolean setRole(int userID, int roleID);
    int getRole(String username);
    boolean setEmployeeID(int userID, int employeeID);
    int getEmployeeID(String username);
    boolean updateUser(UserAccount user);
    boolean deleteUser(UserAccount User);
    void populateDB(String username, String password, int privilege, String email, int employeeID);
}
