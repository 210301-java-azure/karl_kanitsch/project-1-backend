package dev.kkanit.dao;

import dev.kkanit.models.Role;
import io.javalin.http.Context;

public interface RoleDAO {

    Role addRole(Role role);
    String getRoleName(int roleID);
    boolean hasUserAccess(int roleID);
    boolean hasHRAccess(int roleID);
    boolean hasAdminAccess(int roleID);
    boolean deleteRole(Role role);
}
