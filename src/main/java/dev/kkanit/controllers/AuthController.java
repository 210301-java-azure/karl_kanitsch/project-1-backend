package dev.kkanit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kkanit.services.AuthService;
import dev.kkanit.services.RoleService;
import dev.kkanit.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class AuthController {

    private String username = "";
    private String password = "";
    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    private RoleService roleService = new RoleService();
    private AuthService authService = new AuthService();
    private UserService userService = new UserService();
    private ObjectMapper mapper = new ObjectMapper();

    public void handleAuthenticateUser(Context ctx) {
        System.out.println(ctx.formParamMap());
        if (ctx.formParamMap()==null || ctx.formParam("username")==null || ctx.formParam("password")==null) {
            logger.info("missing login credentials");
            throw new UnauthorizedResponse("You must enter a valid Username and Password");
        }
        username = ctx.formParam("username");
        password = ctx.formParam("password");
        logger.info("Authenticating user with username: " + username);
        int roleID = authService.authenticate(username, password);
        int userID = userService.getUserID(username);
        System.out.println("roleID: " + roleID + "\t\tuserID: "+userID);
        if (roleID > 0 && userID > 0) {
            logger.info("User " + username + " authorized with roleID level " + roleID);
            String authToken = authService.generateAuthToken(username, userID, roleID);
            ctx.header("Access-Control-Expose-Headers","auth-token");
            ctx.header("Access-Control-Allow-Origin","*");
//            ctx.header("Vary","Origin");
            ctx.header("auth-token",authToken);
        }
        else {
            logger.warn("Unauthorized login attempt under username: " + username);
            throw new UnauthorizedResponse("Invalid login credentials submitted");
        }
    }

    public void handleLoginGet(Context ctx) {
        String resultString;
        Map headerMap = ctx.headerMap();
        if (headerMap!=null && headerMap.containsKey("auth-token")) {
            String token = (String) headerMap.get("auth-token");
            String authToken = sanitizeJWT(token);
            Map<String, Object> claimsMap = authService.validateAuthToken(authToken);
            logger.info("From JWT: " + claimsMap.toString());
            if (claimsMap.containsKey("username")) {
                resultString = "Welcome, " + claimsMap.get("username") + "\nYou currently have roleID: " + claimsMap.get("roleID");
                try {
                    ctx.json(mapper.writeValueAsString(claimsMap));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
            else {
                throw new UnauthorizedResponse("You must login to perform a get request on this resource.");
            }
        }
        else
            throw new UnauthorizedResponse("You must login to perform a get request on this resource.");

    }

    public String getUsernameFromToken(Context ctx) {
        Map headerMap = ctx.headerMap();
        if (headerMap.containsKey("auth-token")) {
            String token = (String) headerMap.get("auth-token");
            String authToken = sanitizeJWT(token);
            return authService.getUsernameFromToken(authToken);
        }
        return "";
    }

    public int getUserIDFromToken(Context ctx){
        Map headerMap = ctx.headerMap();
        if(headerMap.containsKey("auth-token")){
            String token = (String) headerMap.get("auth-token");
            String authToken = sanitizeJWT(token);
            return authService.getUserIDFromToken(authToken);
        }
        return 0;
    }

    public int getRoleIDFromToken(Context ctx) {
        Map headerMap = ctx.headerMap();
        if (headerMap.containsKey("auth-token")) {
            String token = (String) headerMap.get("auth-token");
            String authToken = sanitizeJWT(token);
            return authService.getRoleIDFromToken(authToken);
        }
        return 0;
    }

    public boolean hasHRAccess(Context ctx) {
        boolean hasAccess = false;
        int roleID = getRoleIDFromToken(ctx);
        hasAccess = roleService.hasHRAccess(roleID);
        return hasAccess;
    }

    public boolean hasUserAccess(Context ctx) {
        boolean hasAccess = false;
        int roleID = getRoleIDFromToken(ctx);
        hasAccess = roleService.hasUserAccess(roleID);
        return hasAccess;
    }

    public boolean hasAdminAccess(Context ctx) {
        boolean hasAccess = false;
        int roleID = getRoleIDFromToken(ctx);
        hasAccess = roleService.hasAdminAccess(roleID);
        return hasAccess;
    }

    public String sanitizeJWT(String authToken){
        return authToken.replaceAll("^\"+|\"+$", "");
    }

}
