package dev.kkanit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kkanit.models.Employee;
import dev.kkanit.services.EmployeeService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class EmployeeController {

    private AuthController authController = new AuthController();
    private EmployeeService employeeService = new EmployeeService();
    private Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private ObjectMapper mapper = new ObjectMapper();

    public void handleGetRequest(Context ctx) {
//        if (authController.hasHRAccess(ctx)) {
//            try {
//                ctx.result(mapper.writeValueAsString(employeeService.getAllEmployees()));
//            } catch (JsonProcessingException e) {
//                e.printStackTrace();
//            }
//        }
//        else
        if (authController.hasUserAccess(ctx)) {
            String user = authController.getUsernameFromToken(ctx);
            try {
                ctx.json(mapper.writeValueAsString(employeeService.getEmployeeByUsername(user)));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        else
            ctx.result("You must log in first to view employee information");
    }

    public void handleGetEmployeeRequest(Context ctx) {
        logger.info("Trying to get employee data for employee id: " + ctx.pathParam("id"));
        int employeeID = 0;
        try {
            employeeID = Integer.parseInt(ctx.pathParam("id"));
        } catch (NumberFormatException e) {
            logger.error("Could not parse " + ctx.pathParam("id") + " to an integer value.");
            return;
        }
        String username = authController.getUsernameFromToken(ctx);
        System.out.println("User: " + username);
        System.out.println(employeeService.getEmployeeIDByUsername(username));
        if (employeeService.getEmployeeIDByUsername(username) == employeeID) {
            try {
                ctx.result(mapper.writeValueAsString(employeeService.getEmployeeByUsername(username)));
                return;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        if (authController.hasHRAccess(ctx)) {
            try {
                ctx.result(mapper.writeValueAsString(employeeService.getEmployeeByID(employeeID)));
                return;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        ctx.result("You are not authorized to view this data");
    }

    public void handleGetAllEmployeesRequest(Context ctx){
        if(!authController.hasHRAccess(ctx))
            throw new UnauthorizedResponse("You are not authorized to view employee data.");
        logger.info("Trying to retrieve all employee records");
        List<Employee> employees = employeeService.getAllEmployees();
        ctx.json(employees);
    }

    public void handleAddEmployeeRequest(Context ctx) {
        if (authController.hasHRAccess(ctx)) {
            Map<String, List<String>> formMap = ctx.formParamMap();
            if (employeeService.addNewEmployee(formMap)) {
                ctx.status(201);
                ctx.result("New employee successfully added.");
            }
            else {
                ctx.status(500);
                ctx.result("There was a problem adding the new employee.");
            }
        }
        else
            throw new UnauthorizedResponse("You do not have permission to hire new employees.");
    }

    public void handleDeleteEmployeeRequest(Context ctx) {
        if (authController.hasHRAccess(ctx)) {
            Map<String, List<String>> formMap = ctx.formParamMap();
            if (employeeService.deleteEmployeeRecord(formMap)) {
                ctx.status(200);
                ctx.result("Employee successfully deleted from database.");
            }
            else {
                ctx.status(500);
                ctx.result("There was a problem deleting the employee.");
            }
        }
        else
            throw new UnauthorizedResponse("You do not have permission to delete employees.");
    }

    public void handleUpdateEmployeeRequest(Context ctx) {
        if(!authController.hasHRAccess(ctx))
            throw new UnauthorizedResponse("You are not authorized to alter employee data.");
        if(!ctx.formParamMap().containsKey("operation"))
            throw new BadRequestResponse("You have made a mistake of some kind.");
        String operation = ctx.formParamMap().get("operation").get(0);
        if(operation.equals("terminate")) {
            try {
                int employeeID = Integer.parseInt(ctx.formParamMap().get("eid").get(0));
                if (employeeService.terminateEmployee(employeeID)) {
                    logger.info("Employee with ID: " + employeeID + " has been terminated.");
                    ctx.status(200);
                    ctx.result("success");
                }
            } catch (NumberFormatException nfe) {
                logger.warn("Unable to parse " + ctx.headerMap().get("eid") + " into an int.");
                throw new BadRequestResponse("We could not parse an employeeID from the request.");
            }
        }
        else if (operation.equals("update")){
//            logger.info("Update detected");
            Map<String, List<String>> formMap = ctx.formParamMap();
            try {
//                logger.info("entering try block");
                int employeeID = Integer.parseInt(ctx.formParamMap().get("eid").get(0));
                logger.info("EmployeeID: " + employeeID);
                if (employeeService.updateEmployeeRecords(employeeID, formMap)) {
                    logger.info("Employee with ID: " + employeeID + " has been updated.");
                    ctx.status(200);
                    ctx.result("success");
                    return;
                }
                ctx.json(formMap);
            } catch (NumberFormatException nfe) {
                logger.warn("Unable to parse " + ctx.headerMap().get("eid") + " into an int.");
                throw new BadRequestResponse("We could not parse an employeeID from the request.");
            }
            throw new BadRequestResponse("We were unable to update the employee.");
        }
    }

}
