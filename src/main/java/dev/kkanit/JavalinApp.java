package dev.kkanit;

import dev.kkanit.controllers.AuthController;
import dev.kkanit.controllers.EmployeeController;
import dev.kkanit.controllers.UserController;
import dev.kkanit.utilities.DBPopulator;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {

    private static final int PORT = 80;

    AuthController authController = new AuthController();
    UserController userController = new UserController();
    EmployeeController employeeController = new EmployeeController();

    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(() -> {
        path("login", () -> {
            get(authController::handleLoginGet);
            post(authController::handleAuthenticateUser);
        });
        path("register", ()->{
            post(userController::handleNewUser);
        });
        path("employee",()->{
           get(employeeController::handleGetRequest);
        });
        path("employees", ()->{
            get(employeeController::handleGetAllEmployeesRequest);
            post(employeeController::handleAddEmployeeRequest);
            delete(employeeController::handleDeleteEmployeeRequest);
            put(employeeController::handleUpdateEmployeeRequest);
            path(":id",()->{
               get(employeeController::handleGetEmployeeRequest);
               put(employeeController::handleUpdateEmployeeRequest);
            });
        });
        path("users", ()->{
           get(userController::handleGetAllUsersRequest);
           post(userController::handlePostUserRequest);
           path(":id", ()->{
               get(userController::handleGetUserRequest);
               path("change-password", ()->{
                   post(userController::handleChangeUserPassword);
               });
            });
        });
        path("users",()->{
//           get(userController::handleTestPull);
        });
        path("db_reset",()->{
           get(DBPopulator::resetDB);
        });
    });

    public void start() {
        this.app.start(PORT);
    }

    public void stop() {
        this.app.stop();
    }
}
