package dev.kkanit.services;

import dev.kkanit.dao.UserDAO;
import dev.kkanit.dao.UserDAOImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

public class AuthService {

    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);
    private static final UserDAO uDAO = new UserDAOImpl();
    private static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    // Verifies a userAccount matching the username and password provided exists
    // and returns the roleID if so, or 0 if not.
    public int authenticate(String username, String password) {
        logger.info("Attempting to authenticate user.");
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        int roleID = 0;
        if (!uDAO.getAllUsernames().contains(username)) {
            logger.info("Unmatched username: " + username + " supplied during login");
        }
        else {
            String dbPassword = uDAO.getUserPassword(username);
//            System.out.println(dbPassword);
//            System.out.println(passwordEncoder.encode(password));
//            System.out.println(passwordEncoder.encode(password));
//            System.out.println(passwordEncoder.encode(password));
            if (dbPassword == null) {
                logger.warn("No password retrieved.");
            } else if (passwordEncoder.matches(password, dbPassword)) {
                roleID = uDAO.getRole(username);
                System.out.println("Passwords Match!");
            }
        }
        return roleID;
    }

    // Takes all passed parameters and generates a JWT containing this information.
    // The JWT is signed with the key and returned as a string.
    public String generateAuthToken(String username, int userID, int roleID) {
        Map<String, String> claimMap = new HashMap<>();
        claimMap.put("username", username);
        claimMap.put("userID", String.valueOf(userID));
        claimMap.put("roleID", Integer.toString(roleID));
        String jws = Jwts.builder().setClaims(claimMap).signWith(key).compact();
        logger.info("Created JWS: " + jws);
        return jws;
    }

    // Verifies the signature on the passed JWT using the key and parses the
    // token contents into a map which is returned.
    public Map<String, Object> validateAuthToken(String authToken) {
        try {
            return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(authToken).getBody();
        } catch (Exception e) {
            logger.error("SignatureException encountered while validating login token.");
        }
        return new HashMap<>();
    }

    // Calls validateAuthToken on the passed JWT and extracts the username
    // from the resultant map.
    public String getUsernameFromToken(String authToken){
        Map<String, Object> claimsMap = validateAuthToken(authToken);
        if(claimsMap.containsKey("username")) {
            logger.info("Username retrieved from token: "+claimsMap.get("username"));
            return (String) claimsMap.get("username");
        }
        else
            return null;
    }

    // Calls validateAuthToken on the passed JWT and extracts the userID
    // from the resultant map.
    public int getUserIDFromToken(String authToken){
        Map<String, Object> claimsMap = validateAuthToken(authToken);
        if(claimsMap.containsKey("userID")){
            return Integer.parseInt((String) claimsMap.get("userID"));
        }
        return 0;
    }

    // Calls validateAuthToken on the passed JWT and extracts the roleID
    // from the resultant map.
    public int getRoleIDFromToken(String authToken){
        Map<String, Object> claimsMap = validateAuthToken(authToken);
        if(claimsMap.containsKey("roleID")) {
            return Integer.parseInt((String) claimsMap.get("roleID"));
        }
        else
            return 0;
    }


    // Uses BCrypt to securely hash passwords. Should be run anytime a new
    // password is submitted before the password is sent to the database.
    public String hashPassword(String password){
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}
