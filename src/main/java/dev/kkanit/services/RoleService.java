package dev.kkanit.services;

import dev.kkanit.dao.RoleDAO;
import dev.kkanit.dao.RoleDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoleService {

    private RoleDAO rDAO = new RoleDAOImpl();
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    public boolean hasHRAccess(int privilege) {
        boolean hasAccess = false;
        hasAccess = rDAO.hasHRAccess(privilege);
        return hasAccess;
    }

    public boolean hasUserAccess(int privilege) {
        boolean hasAccess = false;
        hasAccess = rDAO.hasUserAccess(privilege);
        return hasAccess;
    }

    public boolean hasAdminAccess(int privilege) {
        boolean hasAccess = false;
        hasAccess = rDAO.hasAdminAccess(privilege);
        return hasAccess;
    }
}
