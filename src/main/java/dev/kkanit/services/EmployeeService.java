package dev.kkanit.services;

import dev.kkanit.dao.*;
import dev.kkanit.models.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class EmployeeService {

    private EmployeeDAO eDAO = new EmployeeDAOImpl();
    private UserDAO uDAO = new UserDAOImpl();
    private Logger logger = LoggerFactory.getLogger(EmployeeService.class);

    public List<Employee> getAllEmployees() {
        List<Employee> employees = eDAO.getAllEmployees();
        return employees;
    }

    public Employee getEmployeeByUsername(String username) {
        int employeeID = uDAO.getEmployeeID(username);
        return eDAO.getEmployee(employeeID);
    }

    public int getEmployeeIDByUsername(String username) {
        int employeeID = uDAO.getEmployeeID(username);
        return employeeID;
    }

    public Employee getEmployeeByID(int employeeID) {
        return eDAO.getEmployee(employeeID);
    }

    public boolean addNewEmployee(Map<String, List<String>> formMap) {
        boolean success = false;
        logger.info("Adding new employee");
        if (formMap.containsKey("firstName") &&
                formMap.containsKey("lastName") &&
                formMap.containsKey("payRate") &&
                formMap.containsKey("federalID")) {
            logger.info("form data found");
            String firstName = formMap.get("firstName").get(0);
            String lastName = formMap.get("lastName").get(0);
            Double payRate;
            try {
                payRate = Double.parseDouble(formMap.get("payRate").get(0));
            } catch(NumberFormatException e) {
                logger.error("Could not parse " + formMap.get("payRate").get(0) + " into a double.");
                return false;
            }
            String federalID = formMap.get("federalID").get(0);
            if (eDAO.addEmployee(firstName, lastName, payRate, federalID)!=0)
                success = true;
        }
        return success;
    }

    public boolean deleteEmployeeRecord(Map<String, List<String>> formMap) {
        boolean success = false;
        if (formMap.containsKey("employee-id")) {
            int employeeID = 0;
            try {
                employeeID = Integer.parseInt(formMap.get("employee-id").get(0));
            } catch (NumberFormatException e) {
                logger.error("Could not parse " + formMap.get("employee-id").get(0) + " into an integer");
            }
            if (eDAO.removeEmployee(employeeID))
                success = true;
        }
        logger.info(Boolean.toString(success));
        return success;
    }

    public boolean terminateEmployee(int employeeID){
        Employee employee = eDAO.getEmployee(employeeID);
        String terminationDate = LocalDate.now().toString();
        employee.setActive(false);
        employee.setTerminationDate(terminationDate);
        eDAO.updateEmployee(employee);
        return employee.equals(eDAO.getEmployee(employeeID));
    }

    public boolean updateEmployeeRecords(int employeeID, Map<String,List<String>> formMap) {
        Employee employee = eDAO.getEmployee(employeeID);
        for(String key: formMap.keySet()){
            switch(key){
                case "firstName":
                    employee.setFirstName(formMap.get("firstName").get(0));
                    break;
                case "lastName":
                    employee.setLastName(formMap.get("lastName").get(0));
                    break;
                case "payRate":
                    try {
                        double payRate = Double.parseDouble(formMap.get("payRate").get(0));
                        employee.setPayRate(payRate);
                    } catch (NumberFormatException nfe){
                        logger.warn("Unable to parse payRate: " + formMap.get("payRate").get(0) + " into a double.");
                        return false;
                    }
                    break;
                case "accruedPTO":
                    try {
                        double accruedPTO = Double.parseDouble(formMap.get("accruedPTO").get(0));
                        employee.setPayRate(accruedPTO);
                    } catch (NumberFormatException nfe){
                        logger.warn("Unable to parse accruedPTO: " + formMap.get("accruedPTO").get(0) + " into a double.");
                        return false;
                    }
                    break;
            }
        }
        eDAO.updateEmployee(employee);
        return employee.equals(eDAO.getEmployee(employeeID));
    }
}
